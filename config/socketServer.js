("use strict");

function startSocketServer(server) {
    console.log(server)
    const io = require('socket.io').listen(server);
    console.log('SocketServer started'); //eslint-disable-line
    var users = [];
    function getkeyByValue(object, value) {
        console.log(users, value)
        return Object.keys(object).find(key =>
            object[key] === value)
    }
    io.sockets.on('connection', (socket) => {
        console.log("", socket.id)
        socket.userId = socket.handshake.query.token;

        console.log("user connection Established");
        users[socket.userId] = socket.id;
        console.log("Connected user's", users);
    });
}

module.exports = {
    startSocketServer
};
// module.exports = startSocketServer;