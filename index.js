
const express = require('express');
bodyparser = require("body-parser");
app = express();
cors = require('cors');
const app2 = express();
const mongoose = require('mongoose');
const dotenv= require("dotenv");
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
const socketio = require("socket.io");

dotenv.config();

var fs = require('fs');
var http = require('http');
var https = require('https');
var privateKey  = fs.readFileSync('sslcert/ssl.key', 'utf8');
var certificate = fs.readFileSync('sslcert/ssl.cert', 'utf8');

var credentials = {key: privateKey, cert: certificate};

var httpsServer = https.createServer(credentials, app);

// var httpsServer = https.createServer({
//   key: fs.readFileSync('sslcert/server.key'),
//   cert: fs.readFileSync('sslcert/server.cert')
// }, app);


///////DB Connection ///////////////////
// var certFileBuf = fs.readFileSync('sslcert/ssl.crt', 'utf8');

// var mongodboptions = {
//   sslCA: certFileBuf,
//   useUnifiedTopology: true,
//   useNewUrlParser: true,
//   useFindAndModify: false,
//   useCreateIndex: true  
// } 
// mongoose.connect(process.env.DB_CONNECT, mongodboptions , function(error) {
//   if (error) {
//     console.log(error)
//   }
//   console.log("connected to db mongodb");
// });

mongoose.connect(process.env.DB_CONNECT, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
  serverSelectionTimeoutMS: 5000 }, function(error) {
  if (error) {
    console.log(error)
  }
  console.log("connected to db mongodb");
});

const userauth=require("./Routes/V1/Userauth");


// use the modules
app.use(cors())
app.use(express.static(__dirname + '/public'));
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: true }));

//process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 1;

app.use("/v1/userauth",userauth); 


//app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(openapiSpecification));

app.use('/api-doc/v1', swaggerUi.serveFiles(swaggerDocument), swaggerUi.setup(swaggerDocument));

app.get('/', (req, res) => {
  res.send("Hello World");
})

// SocketIo Code

const port = 3020;
app2.use(cors());
const server1 = http.Server(app2);
const io = socketio(server1, {
    cors: {
        origin: "*",
        methods: ["GET", "POST"],
        allowedHeaders: ["my-custom-header"],
        credentials: true
    }
})
var users = [];
    function getkeyByValue(object, value) {
        console.log(users, value)
        return Object.keys(object).find(key =>
            object[key] === value)
    }
io.on('connection', function (socket) {
  console.log("", socket.id)
  socket.userId = socket.handshake.query.userId;
  console.log("user connection Established");
  users[socket.userId] = socket.id;
  console.log("Connected user's", users);

  socket.on("disconnect", function () {
      ans = getkeyByValue(users, socket.id);
      if (ans) {
          console.log(ans)
          delete users[ans];
          io.emit("user_disconnected", ans);
      }
      console.log("user disconnected", users);
  });

  socket.on("updatePhotographerLocation", function (address) {
    var add = JSON.parse(address);
    console.log("address,",add)
    // var query = [
    //     parseFloat(add.lat_long[1]),
    //     parseFloat(add.lat_long[0]),
    // ]
    socket.broadcast.emit("updatedPhotographerLocation", {
      add
    })
});

});


//

server1.listen(port, () => {
  console.log("Running on Port: " + port);
});
// starting the server  on local host 

app.listen(4050,()=>console.log("running at 4050 port")); 
///// start the server using https /////////
///httpsServer.listen(4050,()=>console.log("running at 4050 port https"));
