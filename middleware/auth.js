const jwt = require("jsonwebtoken");

const HttpRespose = require("../common/httpResponse");
const Logger = require("../common/logger");
const AppCode = require("../common/constant/appCods");

const config = process.env;

const verifyToken = (req, res, next) => {
  const response = new HttpRespose();
  const token =
    req.body.token || req.query.token || req.headers.token;

  if (!token) {
    //return res.status(403).send("A token is required for authentication");
    response.setError(AppCode.TokenRequired);
    return response.send(res);
  }
  try {
    const decoded = jwt.verify(token, config.TOKEN_SECRET);
    req.user = decoded;
  } catch (err) {
    
    response.setError(AppCode.Tokeninvalid);
    return response.send(res);
  }
  return next();
};

module.exports = verifyToken;