
module.exports = {
    RegistrationHTmlTemplete: function (params) {

     const dataURIfb = params.logo;
     //console.log(params.url);
      var HTMLString = '<!DOCTYPE html>'+
      '<html lang="en">'+
      
      '<head>'+
        '<meta charset="UTF-8">'+
        '<meta http-equiv="X-UA-Compatible" content="IE=edge">'+
        '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />'+
        '<meta name="viewport" content="width=device-width, initial-scale=1.0">'+
        '<title>Welcome email</title>'+
        '<link href="https://fonts.googleapis.com/css2?family=Arimo+Sans:wght@400;500&family=Poppins+Sans:wght@600;700&display=swap" rel="stylesheet">'+

        '<style> @media screen and (max-width: 767px) {.text-class {font-size: 18px;}.width-manage {min-width: 15px !important;width: 15px !important;}}</style>'+
      '</head>'+
      
      '<body style="margin: 0; padding: 0; background: #f0f0f0;">'+
        '<table max-width="100%;" align="center" cellpadding="0" cellspacing="0" border="0" style="font-family:  sans-serif; background: #f3f3f3; font-size: 14px; color: #000; width: 100%; min-width: 320px;">'+
         
         ' <tr>'+
            '<td height="30px;" style="font-size: 0; line-height: 0;"></td>'+
          '</tr>'+
          '<tr>'+
           ' <td>'+
               '<table align="center" cellpadding="0" cellspacing="0" border="0" style="max-width:580px; ;width: 600px; min-width: 280px;">'+
               ' <tr>'+
                 ' <td align="center" valign="top">'+
                    '<table cellpadding="0" cellspacing="0" border="0" max-width="600px" style="width: 600px;background: #fff;">'+
      
                      '<tr>'+
                        
                        '<td class="width-manage" style="font-size: 0; line-height: 0; width:30px;" valign="top" align="left"></td>'+
                        '<td>'+
                          '<table width="100%" cellpadding="0" cellspacing="0" border="0">'+
                           ' <tr>'+
                              '<td height="30" style="font-size: 0; line-height: 0;"></td>'+
                            '</tr>'+
                            
                            '<tr>'+
                              '<td style="color: #777777; text-align: left;">'+
                                '<a href="#">'+
                                  '<img src="'+dataURIfb+'" alt="Logo"  style="width: 180px;" >'+
                                '</a>'+
                             ' </td>'+
                            '</tr>'+
                            
                            '<tr>'+
                              '<td height="30" style="font-size: 0; line-height: 0;"></td>'+
                            '</tr>'+
      
                            
                            '<tr>'+
                             ' <td>'+
                                '<table cellpadding="0" cellspacing="0" border="0" width="100%">'+
      
                                 ' <tr>'+
                                    '<td align="left" valign="top" style="line-height: 22px; color: #000000; font-size: 18px; font-family:  sans-serif; font-weight: 700;">'+
                                      'Welcome to the SnapHouss family!'+
                                    '</td>'+
                                  '</tr>'+
                                  
                                  '<tr>'+
                                    '<td height="40" style="font-size: 0; line-height: 0;"></td>'+
                                  '</tr>'+
                                  '<tr>'+
                                    '<td class="text-class" align="left" valign="top" style="line-height: 22px; font-size: 16px; font-family: sans-serif; font-weight: 400; color: #000000;">'+
                                      'Hi '+params.name+','+
                                    '</td>'+
                                  '</tr>'+
                                  
                                  '<tr>'+
                                   ' <td height="20" style="font-size: 0; line-height: 0;"></td>'+
                                  '</tr>'+
                                  '<tr>'+
                                    '<td class="text-class" align="left" valign="top" style="line-height: 22px;text-align: justify; font-size: 16px; font-family:  sans-serif; font-weight: 400; color: #000000;">'+
                                      '<p>You can now take advantage of limited time coupons, loyalty rewards'+ 
                                      'points, cashback on every purchase, track your photographer 2 hours '+
                                      'before the appointment, increase status levels to gain exclusive offers,'+
                                      'know exactly when your content will be ready, instantly download or'+ 
                                      'share them from the app, message customer support directly when you'+ 
                                      'need help and of course, book and manage all your appointments with'+ 
                                      'the click of a button!</p>'+
                                    '</td>'+
                                  '</tr>'+
                                  
                                  '<tr>'+
                                    '<td height="35" style="font-size: 0; line-height: 0;"></td>'+
                                  '</tr>'+
                                 ' <tr>'+
                                  '<td>'+
                                    '<table cellpadding="0" cellspacing="0" border="0" width="100%" style="width: 100%; max-width: 600px; margin: 0 auto;">'+
                                      '<tr>'+
                                        '<td align="center">'+
                                          '<table cellpadding="0" cellspacing="0" border="0" width="100%">'+
                                            '<tr>'+
                                              '<td style="color: #777777; text-align: center; padding-right: 6px;">'+
                                                '<img src="'+params.calendarIcon+'" alt="Calendar icon" width="40" height="40">'+
                                                '<span style="display: block; color: #000000; font-family:  sans-serif; font-size: 11px;line-height: 1.2; font-weight: 700;margin-top: 10px;">Book your appointment</span>'+
                                              '</td>'+
                                              '<td style="color: #777777; text-align: center; padding-right: 6px;">'+
                                                '<img src="'+params.dollerIcon+'" alt="Money icon" width="40" height="40">'+
                                                '<span style="display: block; color: #000000; font-family:  sans-serif; font-size: 11px;line-height: 1.2; font-weight: 700;margin-top: 10px;">Earn loyalty Points</span>'+
                                              '</td>'+
                                              '<td style="color: #777777; text-align: center; padding-right: 6px;">'+
                                                '<img src="'+params.giftIcon+'" alt="Gift icon" width="40" height="40">'+
                                                '<span style="display: block; color: #000000; font-family:  sans-serif; font-size: 11px;line-height: 1.2; font-weight: 700;margin-top: 10px;">Receive incentive</span>'+
                                              '</td>'+
                                              '<td style="color: #777777; text-align: center; padding-right: 6px;">'+
                                                '<img src="'+params.locationIcon+'" alt="Location icon" width="40" height="40">'+
                                                '<span style="display: block; color: #000000; font-family:  sans-serif; font-size: 11px;line-height: 1.2; font-weight: 700;margin-top: 10px;">Track the Photographer</span>'+
                                              '</td>'+
                                              '<td style="color: #777777; text-align: center; padding-right: 6px;">'+
                                                '<img src="'+params.progressIcon+'" alt="Progress icon" width="40" height="40">'+
                                                '<span style="display: block; color: #000000; font-family:  sans-serif; font-size: 11px;line-height: 1.2; font-weight: 700;margin-top: 10px;">Track the Progress</span>'+
                                              '</td>'+
                                              '<td style="color: #777777; text-align: center;">'+
                                                '<img src="'+params.downloadIcon+'" alt="Download icon" width="40" height="40">'+
                                                '<span style="display: block; color: #000000; font-family:  sans-serif; font-size: 11px;line-height: 1.2; font-weight: 700;margin-top: 10px;">Download the all content</span>'+
                                              '</td>'+
                                            '</tr>'+
                                          '</table>'+
                                       '</td>'+
                                      '</tr>'+
                                    '</table>'+
                                  '</td>'+
                                '</tr>'+
                                  
                                  '<tr>'+
                                    '<td height="35" style="font-size: 0; line-height: 0;"></td>'+
                                  '</tr>'+
                                  
                                  '<tr>'+
                                    '<td style="height: 50px; text-align: center;">'+
                                      '<a href="'+params.ShortLink+'" style="color: #ffffff; background-color: #18a02f; font-weight: 600; padding: 11px 55px; text-decoration: none; border-radius: 50px; font-size: 15px; margin: 0 auto; width: 100%; font-family:  sans-serif;">'+
                                        'Book Now'+
                                     ' </a>'+
                                    '</td>'+
                                  '</tr>'+
      
                                  '<tr> <td height="30" style="font-size: 0; line-height: 0;"></td></tr>'+

                                  '<tr>'+
                                    '<td align="left" valign="top" style="font-family:  sans-serif; font-size: 16px; line-height: 20px; color: #000; ">'+
                                      'Kind Regards,'+
                                    '</td>'+
                                  '</tr>'+
                                  
                                  '<tr>'+
                                    '<td align="left" valign="top" style="font-size: 0; line-height: 0" height="5"></td>'+
                                  '</tr>'+
                                  '<tr>'+
                                    '<td align="left" valign="top" style="font-family: sans-serif; font-size: 16px; line-height: 20px; color: #000; ">'+
                                      'The SnapHouss Team'+
                                    '</td>'+
                                  '</tr>'+
                                '</table>'+
                              '</td>'+
                            '</tr>'+

                            '<tr>'+
                              '<td style="height: 30px; font-size: 0;"></td>'+
                            '</tr>'+
                          '</table>'+
                        '</td>'+
                        
                        '<td class="width-manage" style="font-size: 0; line-height: 0; width:30px;" valign="top" align="left">'+
                        '</td>'+
                      '</tr>'+
                    '</table>'+
                  '</td>'+
                '</tr>'+
                
                '<tr>'+
                  '<td style="height: 30px; font-size: 0;"></td>'+
                '</tr>'+
      
                '<tr>'+
                  '<td>'+
                    '<table cellpadding="0" cellspacing="0" border="0" width="100%" style="width: 100%; max-width: 600px; margin: 0 auto;  padding-bottom: 20px;">'+
                      '<tr>'+
                        '<td align="center">'+
                          '<table cellpadding="0" cellspacing="0" border="0" width="100%">'+
                            '<tr>'+
                              '<td style="text-align: center;padding-bottom: 15px;">'+
                                '<a href="#" style="display: inline-block; color: #8e8e8e; text-decoration: none;font-size: 12px;">'+
                                  'Unsubscribe'+
                                '</a>'+
                                '<span style="display: inline-block; color: #8e8e8e;">|</span>'+
                                '<a href="#" style="display: inline-block; color: #8e8e8e; text-decoration: none;font-size: 12px;">'+
                                  'Privacy Policy'+
                                '</a>'+
                                '<span style="display: inline-block; color: #8e8e8e;">|</span>'+
                                '<a href="#" style="display: inline-block; color: #8e8e8e; text-decoration: none;font-size: 12px;">'+
                                  'Contact Support'+
                                '</a>'+
                              '</td>'+
                            '</tr>'+
                          '</table>'+
                        '</td>'+
                      '</tr>'+
                      '<tr>'+
                        '<td align="left" style="color: #8e8e8e;  padding-bottom: 15px; text-align: center;font-size: 12px; font-family:  sans-serif; font-weight: 400;">'+
                          '7030 Woodbine Ave, Suite 500, Markham, ON, Canada L3R 6G2'+
                          '</td>'+
                      '</tr>'+
                      '<tr>'+
                        '<td align="left" style="color: #8e8e8e;  padding-bottom: 15px; text-align: center;font-size: 12px; font-family:  sans-serif; font-weight: 400;">'+
                          '&copy 2022 SnapHouss Franchising Inc.'+
                          '</td>'+
                      '</tr>'+
                    '</table>'+
                  '</td>'+
                '</tr>'+
                
              '</table>'+
            '</td>'+
          '</tr>'+
          
         ' <tr>'+
            '<td height="25px;" style="font-size: 0; line-height: 0;"></td>'+
          '</tr>'+
        '</table>'+
      '</body>'+
      
      '</html>'; 
      
      return HTMLString;
    }
}