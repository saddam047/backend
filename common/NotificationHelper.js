var admin = require('firebase-admin');

const schedule = require('node-schedule');

var serviceAccount = require("../config/serviceAccountKey.json");

admin.initializeApp( {
    credential: admin.credential.cert(serviceAccount)
  }, 
  'first'
);

module.exports = {
    sendnotfication: function(params) {

        try {
            
            const payload = {
                notification: {
                title: "Notification",
                body: params.body,
                }
            };
            
            var options = {
            priority: "high",
            timeToLive: 60 * 60 * 24
            }
            
            // dLmcCJ_JB0BsjTAYIMLbKk:APA91bE9LUKGDv_1J1ACdie2EQrtMFYdQH-aK1M9owL9M7AxXTvR-MgOqTKC6sd3Av1d9JM08OCsgIMhp3Hg8RCZoykylEQoeDqB7aOHoEXpulCd-1PhnGV2LefapfGU4FDORbNhplRa
            //  var token = ['dLmcCJ_JB0BsjTAYIMLbKk:APA91bE9LUKGDv_1J1ACdie2EQrtMFYdQH-aK1M9owL9M7AxXTvR-MgOqTKC6sd3Av1d9JM08OCsgIMhp3Hg8RCZoykylEQoeDqB7aOHoEXpulCd-1PhnGV2LefapfGU4FDORbNhplRa']
            
            var token = params.token;
    
            admin.messaging().sendToDevice(token, payload, options).then(function (response) {
                
                console.log("success",response);
                return response
                
            }).catch(function (error) {
            console.log("Error message sent " + error)
            console.log(error)
            return error;
            })
        } catch (error) {
            return error;
        }
    },
    shedularfunction: function(x) {
        const job = schedule.scheduleJob('*/2 * * * *', function(){
            console.log('The answer to life, the universe, and everything!',x);
            //shedularfunction();
        });       
        return "cron";
    }
  }