const { FirebaseDynamicLinks } = require("firebase-dynamic-links");

const firebaseDynamicLinks = new FirebaseDynamicLinks(process.env.FirebaseDynamicLinksUser);

module.exports = {
    UerDeeplinkWithType:async function(params) {

        try {
            const { shortLink, previewLink } = await firebaseDynamicLinks.createLink({
                dynamicLinkInfo: {
                  link: `${process.env.UserDeeplinkUrl}?type=${params.type}`,
                  domainUriPrefix: ''+process.env.UserDeeplinkUrl+'',
                  androidInfo: {
                    androidPackageName: 'com.SnapHouss',
                  },
                  iosInfo: {
                    iosBundleId: 'com.SnapHouss',
                  },
                },
              });
              return  shortLink ;    
        } catch (error) {
          console.log(error);
            return error;
        }
    },
    UerDeeplinkWithId: function(params) {

        try {
                  
        } catch (error) {
            return error;
        }
    },
    UerDeeplinkWithTypeAndId:async function(params) {

        try {
                
          const { shortLink, previewLink } = await firebaseDynamicLinks.createLink({
            dynamicLinkInfo: {
              link: `${process.env.UserDeeplinkUrl}?_id=${params._id ? params._id : ""}&type=${params.type}`,
              domainUriPrefix: ''+process.env.UserDeeplinkUrl+'',
              androidInfo: {
                androidPackageName: 'com.SnapHouss',
              },
              iosInfo: {
                iosBundleId: 'com.SnapHouss',
              },
            },
          });
          return shortLink;
        } catch (error) {
          console.log(error);
            return error;
        }
    }
  }