const PostalCode = require("../Models/PostalCode");


module.exports = {
    checkPostalCode: async function (params) {

        try {
            if (params) {
                var postalCode = params.postal_code;
                if ((params.countryName).toUpperCase() == 'US') {
                    postalCode = postalCode;
                } else {
                    postalCode = postalCode.substring(0, 3);
                }
                // const chekpostalCode =  PostalCode.findOne({
                //   postal_code: { $regex: new RegExp(postalCode, "i") }
                // }); 
                var results= await PostalCode.aggregate([

                    {
                        $match: {
                            postal_code: { $regex: new RegExp(postalCode, "i") }
                        }
                    },
                    {
                        $lookup: {
                            "from": "acuityaccounts",
                            "localField": "acuity_account_name",
                            "foreignField": "acuity_account_name_key",
                            "as": "acuityaccount"
                        }
                    },
                ])
                if (results[0].acuityaccount.length > 0) {
                    results[0].acuity_key = results[0].acuityaccount[0].acuity_key;
                    results[0].acuity_userId = results[0].acuityaccount[0].acuity_userId;
                    results[0].vat = results[0].acuityaccount[0].vat;
                    results[0].stripeKey = results[0].acuityaccount[0].stripeKey;
                    results[0].online_payment_processing_fee = results[0].acuityaccount[0].online_payment_processing_fee;
                }
                //console.log(results[0].acuityaccount[0]);
                return results[0];

            }

        } catch (error) {
            return error;
        }
    },
    checkPostalCodeByAcuityAccountNAme:async function (params) {

        try {
            if (params) {

                // const chekpostalCode = PostalCode.findOne({
                //     acuity_account_name: params.acuity_account_name
                // });

                var results= await PostalCode.aggregate([

                    {
                        $match: {
                            acuity_account_name: params.acuity_account_name
                        }
                    },
                    {
                        $lookup: {
                            "from": "acuityaccounts",
                            "localField": "acuity_account_name",
                            "foreignField": "acuity_account_name_key",
                            "as": "acuityaccount"
                        }
                    },
                ])
                if (results[0].acuityaccount.length > 0) {
                    results[0].acuity_key = results[0].acuityaccount[0].acuity_key;
                    results[0].acuity_userId = results[0].acuityaccount[0].acuity_userId;
                    results[0].vat = results[0].acuityaccount[0].vat;
                    results[0].stripeKey = results[0].acuityaccount[0].stripeKey;
                    results[0].online_payment_processing_fee = results[0].acuityaccount[0].online_payment_processing_fee;
                }
                //console.log(results[0]);
                return results[0];
            }

        } catch (error) {
            return error;
        }
    }
}