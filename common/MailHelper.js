const nodemailer = require('nodemailer');
var inlineBase64 = require('nodemailer-plugin-inline-base64');
const Logger = require("../../backend/common/logger");
const AppCode = require("../../backend/common/constant/appCods");

module.exports = {
    sendMail: function(params) {

        try {
            if (params) {
              
                var transporter = nodemailer.createTransport({
                    host: 'smtp.gmail.com',
                    port: 465,
                    secureConnection: false,
                    secure: true, // use SSL
                    auth: {
                      user: process.env.EMAIL_USER,
                      pass: process.env.EMAIL_PASSWORD
                    }
                  });
                  transporter.use('compile', inlineBase64({ cidPrefix: 'somePrefix_' }));
                  var mailOptions = {
                    to: params.email_send_to,
                    // to: 'gunjankumardevelopment@gmail.com',
                    from: process.env.EMAIL_USER,
                    subject: params.email_subject,
                    html: params.email_body,
                  }
                  transporter.sendMail(mailOptions, (err, info) => {
                    if (err) {
                      Logger.error(AppCode.InternalServerError.message, err);
                      
                    } else {
        
                      console.log("Mail Sent ",params.email_send_to);
                    }
                  })
            }           
        } catch (error) {
            return error;
        }
    }
  }