const mongoose= require("mongoose");
const { ObjectId } = require("mongodb");
const Schema= new mongoose.Schema({
    apointment_id:{
        type:ObjectId,
        ref:"appointments",
    },
    workFlowStatus_id:{
        type:ObjectId,
        ref:"appointmentstatuses",
    },
    executionTime:Date,
    isExecuted : Boolean,
    remarks : String,
    created_date : Date,
    actualExecutionTime :Date
});

module.exports=mongoose.model("workFlowAutomation",Schema);