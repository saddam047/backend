const mongoose = require("mongoose");
const { ObjectId } = require("mongodb");
const Schema = new mongoose.Schema({
    sender_id: ObjectId,
    reciver_id: ObjectId,
    appointment_id: ObjectId,
    message: String,
    redirect_screen:String,
    redirection:String,
    type: String,
    title: String,
    url: String,
    icon: String,
    adminNotifyKey: String,
    sendto: String,
    completed_percantage: String,
    photographerImage: String,
    photographerName: String,
    isRead: {
        type: Boolean,
        enum: [false, true],
        default: false
    },
    isView: {
        type: Boolean,
        enum: [false, true],
        default: false
    },
    status: {
        type: Number,
        enum: [1, 0],
        default: 1
    },
    delete_flag: {
        type: Number,
        enum: [1, 0],
        default: 0
    },
    created_date: Date,
    modified_date: Date
});

module.exports = mongoose.model("notification", Schema);