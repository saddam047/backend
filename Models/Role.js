const mongoose= require("mongoose");
const { ObjectId } = require("mongodb");
const userSchema= new mongoose.Schema({
    name : String,
    permission: {
        type: Array,
        'default': []
    },
    created_date:Date,
    modified_date:Date,
    delete_flag: {
        type: Number,
        enum : [1,0],
        default: 0
    } ,
    status: {
        type: Number,
        enum : [1,0],
        default: 1
    } 
    
});

module.exports=mongoose.model("Role",userSchema);