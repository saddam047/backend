const mongoose= require("mongoose");
const { ObjectId } = require("mongodb");
const Schema= new mongoose.Schema({

    name:String,
    modified_by : ObjectId,
    status: {
        type: Number,
        enum : [1,0],
        default: 1
    },
    delete_flag: {
        type: Number,
        enum : [1,0],
        default: 0
    },
    created_date:Date,
    modified_date:Date
});

module.exports=mongoose.model("county",Schema);