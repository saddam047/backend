const mongoose = require("mongoose");
const { ObjectId } = require("mongodb");
const userSchema = new mongoose.Schema({
    provider_id: String,
    provider_name: String,
    name: String,
    firstname: String,
    lastname: String,
    email: String,
    phone: String,
    password: String,
    token: String,
    total_loyalty_point: {
        type: Number,
        default: 0
    },
    dob: String,
    gender: String,
    profile_image: String,
    device_type: String,
    device_token: String,
    subscription_plan_id: {
        type: ObjectId,
        ref: "subscription",
    },
    subscription_cancel: String,
    status: {
        type: Number,
        enum: [1, 0],
        default: 1
    },
    Is_new_booing: {
        type: Boolean,
        default: false
    },
    block_by_admin: {
        type: Boolean,
        default: false
    },
    registration_date: Date,
    googleAuthCode: String,
    googleId: String,
    isGoogleLogin: Boolean,
});

module.exports = mongoose.model("User", userSchema);