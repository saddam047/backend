const mongoose= require("mongoose");
const { ObjectId } = require("mongodb");
const Schema= new mongoose.Schema({

    user_id:ObjectId,
    appointment_id:ObjectId,
    punctuality:{
        type: Number,
        default: 1
    },
    speed:{
        type: Number,
        default: 1
    },
    attention_to_details:{
        type: Number,
        default: 1
    },
    customer_service:{
        type: Number,
        default: 1
    },
    professionalism:{
        type: Number,
        default: 1
    },
    status: {
        type: Number,
        enum : [1,0],
        default: 1
    },
    delete_flag: {
        type: Number,
        enum : [1,0],
        default: 0
    },
    created_date:Date,
    modified_date:Date
});

module.exports=mongoose.model("feedback",Schema);