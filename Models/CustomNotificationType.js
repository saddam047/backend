const mongoose= require("mongoose");
const { ObjectId } = require("mongodb");
const Schema= new mongoose.Schema({
    title:String,
    message : String,
    order : Number
});

module.exports=mongoose.model("customNotificationType",Schema,"customNotificationType");