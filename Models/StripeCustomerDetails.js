const mongoose = require("mongoose");
const { ObjectId } = require("mongodb");
const Schema = new mongoose.Schema({
    user_id: ObjectId,
    stripe_customer_id: String,
    card_number:String,
    acuity_account_name:String,
    countryName:String,
    postalCode_id: ObjectId,
    created_date: Date,
    modified_date: Date,
    stripe_response: Object
});

module.exports = mongoose.model("stripeCustomerDetails", Schema);