const mongoose = require('mongoose');


const notificationIconSchema = new mongoose.Schema({
    title: { type: String },
    path: { type: String },
});

// ("customNotificationType", Schema, "customNotificationType");
module.exports = mongoose.model('notificationIcon', notificationIconSchema, "notificationIcon");