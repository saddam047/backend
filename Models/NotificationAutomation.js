const mongoose= require("mongoose");
const { ObjectId } = require("mongodb");
const Schema= new mongoose.Schema({
    user_id:{
        type:ObjectId,
        ref:"users",
    },
    customNotificationType_id:{
        type:ObjectId,
        ref:"customNotificationType",
    },
    executionTime:Date,
    isExecuted : Boolean,
    remarks : String,
    created_date : Date,
    actualExecutionTime :Date
});

module.exports=mongoose.model("notificationAutomation",Schema);