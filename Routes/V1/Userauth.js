const User = require("../../Models/User");
const bcript = require("bcryptjs");
const crypto = require('crypto');
const router = require("express").Router();
var generator = require('generate-password');
const jwt = require("jsonwebtoken");
const nodemailer = require('nodemailer');
const multer = require('multer');
var inlineBase64 = require('nodemailer-plugin-inline-base64');
const MailHelper = require('../../common/MailHelper');
const UserDeeplinkHelper = require('../../common/DeeplinkHelper/UserDeeplinkHelper');
const WelcomeEmailTemplete = require('../../common/EmailTemplete/WelcomeEmailTemplete');
const RegistrationTemplete = require('../../common/EmailTemplete/RegistrationTemplete');
const ResetPasswordTemplete = require('../../common/EmailTemplete/ResetPasswordTemplete');
const auth = require("../../middleware/auth");
const { ObjectId } = require("mongodb");
const { FirebaseDynamicLinks } = require("firebase-dynamic-links");
const HttpRespose = require("../../common/httpResponse");
const Logger = require("../../common/logger");
const AppCode = require("../../common/constant/appCods");
const fs = require('fs');

const Notification = require("../../Models/Notification");
const admin = require("firebase-admin");

const ResetPassword = require("../../Models/ResetPassword");
const NotificationAutomation = require("../../Models/NotificationAutomation");
const CustomNotificationType = require("../../Models/CustomNotificationType");

var generator = require('generate-password');

var multiparty = require('connect-multiparty'),
  multipartyMiddleware = multiparty({ uploadDir: './imagesPath' });
const DIR = './public/profilePics/';
if (!fs.existsSync(DIR)) {
  fs.mkdirSync(DIR, { recursive: true });
}
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, DIR);
  },
  filename: (req, file, cb) => {
    const fileName = file.originalname.toLowerCase().split(' ').join('-');
    cb(null, fileName)
  }
});


var serviceAccountforApp = require("../../config/serviceAccountKey.json");


var appAdmin = admin.initializeApp(
  {
    credential: admin.credential.cert(serviceAccountforApp)
  },
  'six' // this name will be used to retrieve firebase instance. E.g. first.database();
);

var upload = multer({
  storage: storage,

  fileFilter: (req, file, cb) => {
    cb(null, true);
  }
});

const firebaseDynamicLinks = new FirebaseDynamicLinks(process.env.FirebaseDynamicLinksUser);

router.post('/login', multipartyMiddleware, async (req, res) => {
  const response = new HttpRespose();
  try {



    if (!req.body.email || req.body.email == "") {
      response.setError(AppCode.EmptyEmail);
      return response.send(res);
    }
    if (!req.body.password || req.body.password == "") {
      response.setError(AppCode.EmptyPassword);
      return response.send(res);
    }

    const user = await User.findOne({
      email: req.body.email
    });
    if (!user) {
      response.setError(AppCode.ExistNotEmail);
      return response.send(res);
    }
    //// check password 
    const validpass = await bcript.compare(req.body.password, user.password)
    if (!validpass) {
      response.setError(AppCode.InvalidCredential);
      return response.send(res);
    }
    var doc = user;
    if (doc.block_by_admin == true) {
      response.setError(AppCode.UserNotActivated);
      return response.send(res);
    } else {
      var url = req.protocol + '://' + req.get('host') + "/profilePics/"
      if (doc.profile_image != null && doc.profile_image != undefined && doc.profile_image != "") {
        doc.profile_image = url + doc.profile_image
      }

      const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET);
      doc.token = token;
      doc.device_token = req.body.device_token;
      doc.device_type = req.body.device_type;
      const filter = { email: req.body.email };
      const update = {};
      update.token = token;
      update.device_type = req.body.device_type;
      update.device_token = req.body.device_token;

      let tokenupdate = await User.findOneAndUpdate(filter, update, { new: true });
      res.header("auth-token", token);
      response.setData(AppCode.LoginSuccess, doc);
      return response.send(res);
    }
  } catch (error) {
    Logger.error(AppCode.InternalServerError.message, error);
    response.setError(AppCode.InternalServerError);
    return response.send(res);
  }

});

router.post('/logout', auth, multipartyMiddleware, async (req, res) => {
  const response = new HttpRespose();
  try {
    console.log(req.user);
    var _id = req.user._id
    var jsondata = {};
    jsondata.device_token = "";
    jsondata.token = "";

    await User.findOneAndUpdate({
      _id: ObjectId(_id)
    }, jsondata, { upsert: true, new: true }, function (err, updatedData) {
      if (err) {
        Logger.error(AppCode.InternalServerError.message, err);
        response.setError(AppCode.InternalServerError);
        return response.send(res);
      } else {
        response.setData(AppCode.Logout, {});
        return response.send(res);
      }
    });
  } catch (error) {
    Logger.error(AppCode.InternalServerError.message, error);
    response.setError(AppCode.InternalServerError);
    return response.send(res);
  }
});

router.post('/userRegistration', multipartyMiddleware, async (req, res) => {
  const response = new HttpRespose();
  try {

    const existemail = await User.findOne({
      email: req.body.email,
    });
    if (existemail) {
      response.setError(AppCode.ExistUser);
      return response.send(res);
    }

    const salt = await bcript.genSalt(10);
    const hashedpassword = await bcript.hash(req.body.password, salt);
    var fullname = '';
    if (req.body.firstname) {
      fullname = req.body.firstname;
    }
    if (req.body.lastname) {
      fullname = fullname + " " + req.body.lastname;
    }
    const user = new User({
      name: fullname,
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      email: req.body.email,
      password: hashedpassword,
      total_loyalty_point: 0,
      device_type: req.body.device_type,
      device_token: req.body.device_token,
      registration_date: Date.now()
    });
    const saveUser = await user.save();
    const customNotificationTypeData = await CustomNotificationType.findOne({ "order": 1 });
    console.log("customNotificationTy", customNotificationTypeData)
    // let executionTime = new Date(new Date().setHours(new Date().getHours() + 3));
    let executionTime = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000)
    const notificationAutomation = new NotificationAutomation({
      user_id: saveUser._id,
      customNotificationType_id: customNotificationTypeData._id,
      isExecuted: false,
      executionTime: executionTime,
      created_date: new Date()
    });
    const saveNotificationAutomation = await notificationAutomation.save();

    const notifi = new Notification({
      reciver_id: saveUser._id,
      message: process.env.FirstAppointmentMSG,
      type: "newUser",
      isRead: false,
      isView: false,
      status: 1,
      delete_flag: 0,
      created_date: new Date(),
      modified_date: new Date()
    });
    const savenotif = await notifi.save();
    let msg = process.env.FirstAppointmentMSG,
      title = process.env.newUserTitle,
      type = 'newUser',
      resData = ""
    // req.body.device_token = 'f1uT-ftUIEX8ptRRtbKQ89:APA91bFk_pLzEm52F1AKWekYkzSf_ZbX5AXNtQyT--R7Ibpl41qrhiJ_P0bQFzq_w_4v5uRrDKQpy5WjeFWmyootJ_bBuJHZsVZPQ9wuyqaV_YToQjHXY_ie2bMjo84LjKoYfy-bySu2'
    if (!!req.body.device_token) {
      console.log("DT", req.body.device_token)
      let tokens = req.body.device_token;
      sendNotification(msg, title, type, tokens, resData)
    }
    console.log(req.body);
    if (req.body.email) {

      /////////////// Welcome Email ////////////////////////////////////////
      templeteparams = {};
      var deeplinkParams = {}
      deeplinkParams.type = process.env.bookAppointment_DeeplinkTpe;
      var ShortLink = await UserDeeplinkHelper.UerDeeplinkWithType(deeplinkParams);
      templeteparams.ShortLink = ShortLink;
      var logofb = fs.readFileSync('./EmailTempleteImage/logo.png');
      var base64Imagelogo = new Buffer.from(logofb).toString('base64');
      var dataURIfb = 'data:image/jpeg;base64,' + base64Imagelogo;
      templeteparams.logo = dataURIfb;
      var calendarIcon = fs.readFileSync('./EmailTempleteImage/calendar-icon.jpeg');
      calendarIcon = new Buffer.from(calendarIcon).toString('base64');
      calendarIcon = 'data:image/jpeg;base64,' + calendarIcon;
      templeteparams.calendarIcon = calendarIcon;

      var dollerIcon = fs.readFileSync('./EmailTempleteImage/doller-icon.jpeg');
      dollerIcon = new Buffer.from(dollerIcon).toString('base64');
      dollerIcon = 'data:image/jpeg;base64,' + dollerIcon;
      templeteparams.dollerIcon = dollerIcon;

      var giftIcon = fs.readFileSync('./EmailTempleteImage/gift-box.png');
      giftIcon = new Buffer.from(giftIcon).toString('base64');
      giftIcon = 'data:image/jpeg;base64,' + giftIcon;
      templeteparams.giftIcon = giftIcon;

      var locationIcon = fs.readFileSync('./EmailTempleteImage/location-icon40.png');
      locationIcon = new Buffer.from(locationIcon).toString('base64');
      locationIcon = 'data:image/jpeg;base64,' + locationIcon;
      templeteparams.locationIcon = locationIcon;

      var progressIcon = fs.readFileSync('./EmailTempleteImage/progress-icon40.png');
      progressIcon = new Buffer.from(progressIcon).toString('base64');
      progressIcon = 'data:image/jpeg;base64,' + progressIcon;
      templeteparams.progressIcon = progressIcon;

      var downloadIcon = fs.readFileSync('./EmailTempleteImage/download-icon40.png');
      downloadIcon = new Buffer.from(downloadIcon).toString('base64');
      downloadIcon = 'data:image/jpeg;base64,' + downloadIcon;
      templeteparams.downloadIcon = downloadIcon;

      templeteparams.name = req.body.firstname;
      var email_body = WelcomeEmailTemplete.RegistrationHTmlTemplete(templeteparams);
      var params = {};
      params.email_send_to = req.body.email
      params.email_subject = process.env.Welcome_Email_Subject
      params.email_body = email_body
      MailHelper.sendMail(params);
      ///////////////////////// New Registration Email ///////////////////////////
      var deeplinkParams = {}
      deeplinkParams.type = process.env.login_DeeplinkTpe;
      deeplinkParams._id = saveUser._id;
      var ShortLink = await UserDeeplinkHelper.UerDeeplinkWithTypeAndId(deeplinkParams);
      templeteparams = {};
      templeteparams.ShortLink = ShortLink;
      logofb = fs.readFileSync('./EmailTempleteImage/logo.png');
      base64Imagelogo = new Buffer.from(logofb).toString('base64');
      dataURIfb = 'data:image/jpeg;base64,' + base64Imagelogo;
      templeteparams.logo = dataURIfb;
      templeteparams.name = req.body.firstname;
      templeteparams.email = req.body.email;
      templeteparams.password = req.body.password;
      var email_body = RegistrationTemplete.RegistrationHTmlTemplete(templeteparams);
      var params = {};
      params.email_send_to = req.body.email
      params.email_subject = process.env.New_Registration_Subject;
      params.email_body = email_body
      MailHelper.sendMail(params);

      const token = jwt.sign({ _id: saveUser._id }, process.env.TOKEN_SECRET);
      saveUser.token = token;
      response.setData(AppCode.UserRegistrationSuccess, saveUser);
      return response.send(res);
    }

  } catch (error) {
    Logger.error(AppCode.InternalServerError.message, error);
    response.setError(AppCode.InternalServerError);
    return response.send(res);
  }
});

router.post('/socialLoginSignUp', multipartyMiddleware, async (req, res) => {
  const response = new HttpRespose();
  try {

    let query = {};
    let data = {}
    if (!req.body.email || req.body.email == "") {
      response.setError(AppCode.EmptyEmail);
      return response.send(res);
    }
    if (req.body.SocialMediaName == 'google') {
      data.isGoogleLogin = true;
      data.googleAuthCode = req.body.AuthCode;
      data.googleId = req.body.googleId;
      query.googleId = req.body.googleId;
    }
    const user = await User.findOne({
      email: req.body.email
    });
    if (!user) {
      var password = generator.generate({
        length: 10,
        numbers: true
      });
      const salt = await bcript.genSalt(10);
      const hashedpassword = await bcript.hash(password, salt);

      const userData = new User({
        name: req.body.firstname + " " + req.body.lastname,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        password: hashedpassword,
        total_loyalty_point: 0,
        device_type: req.body.device_type,
        device_token: req.body.device_token,
        registration_date: Date.now(),
        googleId: data.googleId,
        googleAuthCode: data.googleAuthCode,
        isGoogleLogin: data.isGoogleLogin
      });
      const saveUser = await userData.save();
      if (req.body.email) {
        templeteparams = {};
        var deeplinkParams = {}
        deeplinkParams.type = process.env.bookAppointment_DeeplinkTpe;
        var ShortLink = await UserDeeplinkHelper.UerDeeplinkWithType(deeplinkParams);
        templeteparams.ShortLink = ShortLink;
        var logofb = fs.readFileSync('./EmailTempleteImage/logo.png');
        var base64Imagelogo = new Buffer.from(logofb).toString('base64');
        var dataURIfb = 'data:image/jpeg;base64,' + base64Imagelogo;
        templeteparams.logo = dataURIfb;
        var calendarIcon = fs.readFileSync('./EmailTempleteImage/calendar-icon.jpeg');
        calendarIcon = new Buffer.from(calendarIcon).toString('base64');
        calendarIcon = 'data:image/jpeg;base64,' + calendarIcon;
        templeteparams.calendarIcon = calendarIcon;

        var dollerIcon = fs.readFileSync('./EmailTempleteImage/doller-icon.jpeg');
        dollerIcon = new Buffer.from(dollerIcon).toString('base64');
        dollerIcon = 'data:image/jpeg;base64,' + dollerIcon;
        templeteparams.dollerIcon = dollerIcon;

        var giftIcon = fs.readFileSync('./EmailTempleteImage/gift-box.png');
        giftIcon = new Buffer.from(giftIcon).toString('base64');
        giftIcon = 'data:image/jpeg;base64,' + giftIcon;
        templeteparams.giftIcon = giftIcon;

        var locationIcon = fs.readFileSync('./EmailTempleteImage/location-icon40.png');
        locationIcon = new Buffer.from(locationIcon).toString('base64');
        locationIcon = 'data:image/jpeg;base64,' + locationIcon;
        templeteparams.locationIcon = locationIcon;

        var progressIcon = fs.readFileSync('./EmailTempleteImage/progress-icon40.png');
        progressIcon = new Buffer.from(progressIcon).toString('base64');
        progressIcon = 'data:image/jpeg;base64,' + progressIcon;
        templeteparams.progressIcon = progressIcon;

        var downloadIcon = fs.readFileSync('./EmailTempleteImage/download-icon40.png');
        downloadIcon = new Buffer.from(downloadIcon).toString('base64');
        downloadIcon = 'data:image/jpeg;base64,' + downloadIcon;
        templeteparams.downloadIcon = downloadIcon;

        templeteparams.name = req.body.firstname;
        var email_body = WelcomeEmailTemplete.RegistrationHTmlTemplete(templeteparams);
        var params = {};
        params.email_send_to = req.body.email
        params.email_subject = process.env.Welcome_Email_Subject
        params.email_body = email_body
        MailHelper.sendMail(params);
        ///////////////////////// New Registration Email ///////////////////////////
        var deeplinkParams = {}
        deeplinkParams.type = process.env.login_DeeplinkTpe;
        deeplinkParams._id = saveUser._id;
        var ShortLink = await UserDeeplinkHelper.UerDeeplinkWithTypeAndId(deeplinkParams);
        templeteparams = {};
        templeteparams.ShortLink = ShortLink;
        logofb = fs.readFileSync('./EmailTempleteImage/logo.png');
        base64Imagelogo = new Buffer.from(logofb).toString('base64');
        dataURIfb = 'data:image/jpeg;base64,' + base64Imagelogo;
        templeteparams.logo = dataURIfb;
        templeteparams.name = req.body.firstname;
        templeteparams.email = req.body.email;
        templeteparams.password = password;
        var email_body = RegistrationTemplete.RegistrationHTmlTemplete(templeteparams);
        var params = {};
        params.email_send_to = req.body.email
        params.email_subject = process.env.New_Registration_Subject
        params.email_body = email_body
        MailHelper.sendMail(params);
      }
      var doc = saveUser;
      var url = req.protocol + '://' + req.get('host') + "/profilePics/"
      if (doc.profile_image != null && doc.profile_image != undefined && doc.profile_image != "") {
        doc.profile_image = url + doc.profile_image
      }
      const token = jwt.sign({ _id: saveUser._id }, process.env.TOKEN_SECRET);
      doc.token = token;
      const filter = { email: req.body.email };
      const update = {};
      update.token = token;
      let tokenupdate = await User.findOneAndUpdate(filter, update, { new: true });
      res.header("auth-token", token);
      response.setData(AppCode.UserRegistrationSuccess, doc);
      return response.send(res);
    }
    else {
      let isAuthCorrect = false;
      if (req.body.SocialMediaName == 'google' && user.isGoogleLogin == true) {
        if (user.googleId == req.body.googleId) {
          isAuthCorrect = true;
        }
        if (isAuthCorrect == true) {
          var doc = user;
          var url = req.protocol + '://' + req.get('host') + "/profilePics/"
          if (doc.profile_image != null && doc.profile_image != undefined && doc.profile_image != "") {
            doc.profile_image = url + doc.profile_image
          }
          const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET);
          doc.token = token;
          doc.device_token = req.body.device_token;
          doc.device_type = req.body.device_type;
          doc.googleId = data.googleId;
          doc.googleAuthCode = data.googleAuthCode;
          doc.isGoogleLogin = data.isGoogleLogin;
          const filter = { email: req.body.email };
          const update = {};
          update.token = token;
          update.device_type = req.body.device_type;
          update.device_token = req.body.device_token;
          update.googleId = data.googleId;
          update.googleAuthCode = data.googleAuthCode;
          update.isGoogleLogin = data.isGoogleLogin;
          let tokenupdate = await User.findOneAndUpdate(filter, update, { new: true });
          res.header("auth-token", token);
          response.setData(AppCode.LoginSuccess, doc);
          return response.send(res);
        }
        else {
          response.setError(AppCode.AuthCodeIncorrect);
          return response.send(res);
        }
      }
      else {
        var doc = user;
        var url = req.protocol + '://' + req.get('host') + "/profilePics/"
        if (doc.profile_image != null && doc.profile_image != undefined && doc.profile_image != "") {
          doc.profile_image = url + doc.profile_image
        }
        const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET);
        doc.token = token;
        doc.device_token = req.body.device_token;
        doc.device_type = req.body.device_type;
        doc.googleId = data.googleId;
        doc.googleAuthCode = data.googleAuthCode;
        doc.isGoogleLogin = data.isGoogleLogin;
        const filter = { email: req.body.email };
        const update = {};
        update.token = token;
        update.device_type = req.body.device_type;
        update.device_token = req.body.device_token;
        update.googleId = data.googleId;
        update.googleAuthCode = data.googleAuthCode;
        update.isGoogleLogin = data.isGoogleLogin;
        let tokenupdate = await User.findOneAndUpdate(filter, update, { new: true });
        res.header("auth-token", token);
        response.setData(AppCode.UserRegistrationSuccess, doc);
        return response.send(res);
      }
    }

  } catch (error) {
    Logger.error(AppCode.InternalServerError.message, error);
    response.setError(AppCode.InternalServerError);
    return response.send(res);
  }

});

router.post("/forgotpassword", multipartyMiddleware, async (req, res) => {
  const response = new HttpRespose();
  try {



    if (!req.body.email) {
      response.setError(AppCode.EmptyEmail);
      return response.send(res);
    }
    const user = await User.findOne({
      email: req.body.email
    });
    if (!user) {
      response.setError(AppCode.ExistNotEmail);
      return response.send(res);
    }
    var resettoken = new ResetPassword({ _userId: user._id, resettoken: crypto.randomBytes(16).toString('hex') });
    resettoken.save(async function (err) {
      if (err) {
        Logger.error(AppCode.InternalServerError.message, err.message);
        response.setError(AppCode.InternalServerError + " " + err.message);
        return response.send(res);
      }
      ResetPassword.find({ _userId: user._id, resettoken: { $ne: resettoken.resettoken } }).remove().exec();
      // const { shortLink, previewLink } = await firebaseDynamicLinks.createLink({
      //   dynamicLinkInfo: {
      //     domainUriPrefix: 'https://snaphouss.page.link?type=resetPassword&token='+resettoken.resettoken+'',
      //     link: 'https://snaphouss.page.link/',
      //     androidInfo: {
      //       androidPackageName: 'com.SnapHouss',
      //     },
      //     iosInfo: {
      //       iosBundleId: 'com.SnapHouss',
      //     },
      //   },
      // });
      const { shortLink, previewLink } = await firebaseDynamicLinks.createLink({
        dynamicLinkInfo: {
          link: `https://snaphouss.page.link?token=${resettoken.resettoken ? resettoken.resettoken : ""}&type=newPassword`,
          domainUriPrefix: 'https://snaphouss.page.link',
          androidInfo: {
            androidPackageName: 'com.SnapHouss',
          },
          iosInfo: {
            iosBundleId: 'com.SnapHouss',
          },
        },
      });

      // const { shortLink, previewLink } = await firebaseDynamicLinks.createLink({
      //   longDynamicLink: 'https://snaphouss.page.link/?link=https://www.w3schools.com/&isi=1583973477&ibi=com.SnapHouss',
      // }); 


      templeteparams = {};
      var logofb = fs.readFileSync('./EmailTempleteImage/logo.png');
      var base64Imagelogo = new Buffer.from(logofb).toString('base64');
      var dataURIfb = 'data:image/jpeg;base64,' + base64Imagelogo;
      templeteparams.logo = dataURIfb;
      templeteparams.name = user.firstname;
      templeteparams.shortLink = shortLink;
      var email_body = ResetPasswordTemplete.ResetPasswordHTmlTemplete(templeteparams);
      var params = {};
      params.email_send_to = req.body.email
      params.email_subject = process.env.Reset_Password_Subject
      params.email_body = email_body
      MailHelper.sendMail(params);
      response.setData(AppCode.ForgotPasswordSuccess, {});
      return response.send(res);
    });

  } catch (error) {
    Logger.error(AppCode.InternalServerError.message, error);
    response.setError(AppCode.InternalServerError);
    return response.send(res);
  }

});

router.post('/resetpassword', multipartyMiddleware, async (req, res) => {
  const response = new HttpRespose();
  try {

    if (!req.body.resettoken) {
      response.setError(AppCode.ResetToken);
      return response.send(res);
    }
    if (req.body.password != req.body.conpassword) {
      response.setError(AppCode.ConfirmPasswordNotMatch);
      return response.send(res);
    }
    const user = await ResetPassword.findOne({
      resettoken: req.body.resettoken
    });
    if (!user) {
      response.setError(AppCode.InvalidResetToken);
      return response.send(res);
    }
    User.findOne({ _id: user._userId }).then(() => {
      ResetPassword.findOne({ resettoken: req.body.resettoken }, function (err, userToken, next) {
        if (!userToken) {
          response.setError(AppCode.ExpiredResetToken);
          return response.send(res);
        }
        User.findOne({ _id: userToken._userId }, function (err, userEmail, next) {
          if (!userEmail) {
            response.setError(AppCode.NoUserFound);
            return response.send(res);
          }
          return bcript.hash(req.body.password, 10, (err, hash) => {
            if (err) {
              response.setError(AppCode.ErrorHashing);
              return response.send(res);
            }
            userEmail.password = hash;
            userEmail.save(function (err) {
              if (err) {
                response.setError(AppCode.PasswordCantReset);
                return response.send(res);
              } else {
                userToken.remove();
                response.setData(AppCode.ResetPasswordSuccess, {});
                return response.send(res);
              }

            });
          });
        });
      });
    }).catch((err) => {
      Logger.error(AppCode.InternalServerError.message, err);
      response.setError(AppCode.InternalServerError + err);
      return response.send(res);
    });
  } catch (error) {
    Logger.error(AppCode.InternalServerError.message, error);
    response.setError(AppCode.InternalServerError);
    return response.send(res);
  }
});

router.post('/changePassword', multipartyMiddleware, async (req, res) => {
  const response = new HttpRespose();
  try {
    const user = await User.findOne({
      email: req.body.email
    });

    if (!user) {
      response.setError(AppCode.ExistNotEmail);
      return response.send(res);
    }

    //// check password 
    if (req.body.repeatPassword != req.body.newPassword) {
      response.setError(AppCode.ConfirmPasswordNotMatch);
      return response.send(res);
    }
    const validpass = await bcript.compare(req.body.oldPassword, user.password)
    if (!validpass) {
      response.setError(AppCode.oldPasswordNotMatch);
      return response.send(res);
    }
    const salt = await bcript.genSalt(10);
    const hashedpassword = await bcript.hash(req.body.newPassword, salt);
    User.findByIdAndUpdate(user._id, { password: hashedpassword }, function (err, docs) {
      if (err) {
        Logger.error(AppCode.InternalServerError.message, err);
        response.setError(AppCode.InternalServerError);
        return response.send(res);
      }
      else {
        var transporter = nodemailer.createTransport({
          service: 'Gmail',
          port: 465,
          auth: {
            user: process.env.EMAIL_USER,
            pass: process.env.EMAIL_PASSWORD
          }
        });
        transporter.use('compile', inlineBase64({ cidPrefix: 'somePrefix_' }));
        var mailOptions = {
          to: req.body.email,
          // to: 'gunjankumardevelopment@gmail.com',
          from: process.env.EMAIL_USER,
          subject: "Reset Password",
          text: "Hi " + docs.name + " your new Password has been set " + req.body.newPassword,
        }
        transporter.sendMail(mailOptions, (err, info) => {
          if (err) {
            Logger.error(AppCode.InternalServerError.message, err);
            response.setError(AppCode.InternalServerError);
            return response.send(res);
          } else {
            response.setData(AppCode.PasswordChnage, {});
            return response.send(res);
          }
        })
      }
    });
  } catch (error) {
    Logger.error(AppCode.InternalServerError.message, error);
    response.setError(AppCode.InternalServerError);
    return response.send(res);
  }
});


module.exports = router;